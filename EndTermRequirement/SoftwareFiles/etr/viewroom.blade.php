<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Room</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<style>
    body {
    text-align: center;
    
}
form {
    display: inline-block;
    border-radius: 10px;
    margin: 10px;
    padding: 50px;
    border: 1px solid;
}
</style>
<body>
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">View Room Details Page</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      
    </ul>
    <a class="nav-link" href="/clients">Clients<span class="sr-only">(current)</span></a>
    <a class="nav-link" href="/home">Home<span class="sr-only">(current)</span></a>
    <a class="btn btn-danger" href="/login">Logout<span class="sr-only">(current)</span></a>
  </div>
</nav>
@foreach($users as $user)
<form >
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <label for="username">Room No.</label>
        <input type="text" name="username" class="form-control" placeholder="{{ $user->rooms_no}}" disabled>
   
    
        <label for="password">Client Name</label>
        <input type="text"  name="password" class="form-control" placeholder="{{ $user->client_name}}" disabled>

        <label for="password">Client Phone Number</label>
        <input type="text"  name="password" class="form-control" placeholder="{{ $user->client_phone}}" disabled>
     <br>   
     <a class="btn btn-info" href="/editroom/{{ $user->rooms_id }}">Edit</a>
    
    </form>
  @endforeach


</body>
</html>