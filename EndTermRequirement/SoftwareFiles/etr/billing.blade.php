<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Billing</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<style>
    body {
    text-align: center;
    
}
form {
    display: inline-block;
    border-radius: 10px;
    margin: 10px;
    padding: 50px;
    border: 1px solid;
}
</style>
<body>
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Billing Page</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
         
      </li>
      
    </ul>
    <a class="nav-link" href="/home">Home<span class="sr-only">(current)</span></a>
    <a class="btn btn-danger" href="/home">Back<span class="sr-only">(current)</span></a>
  </div>
</nav>
@foreach($users as $user)

<form action = "/compute/{{ $user->rooms_id}}" method = "post">
<h5>{{ $user->client_name}}</h5>

     <h5>Room {{ $user->rooms_id}}</h5>
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
  
        <label for="username">Enter Date of Payment</label>
        <input type="date" name="date" class="form-control" >

        <label for="username">Months Paid</label>
        <input type="text" name="months" class="form-control" >
  
        <label for="password">Enter KWH</label>
        <input type="text"  name="kwh" class="form-control" >

        <label for="password">Rent Fee</label>
        <input type="text"  name="rent" class="form-control" value="{{$user->rent}}" readonly>
        <label for="password">Water Fee</label>
        <input type="text"  name="water" class="form-control" value="{{$user->water}}" readonly>
        <label for="password">Electric Fee</label>
        <input type="text"  name="electric" class="form-control" value="{{$user->electric}}" readonly>
     <br>   
     <input class="btn btn-secondary" type = 'submit' value = "Compute" />
     <br>
     
    </form>
    @endforeach
</body>
</html>