<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clients Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<style>
  body {
    text-align: center;
    
}
table {
  width:50%;
    table-layout: fixed;
    display: inline-block;
    border-radius: 10px;
    padding: 20px;
    border: 1px solid;
    margin-right: auto;
    margin-left: auto;
    font-size: 15px;
}
td {
  margin: 10px;
  padding: 20px;
  border-bottom: 1px solid;
}

th {
  margin: 10px;
  padding: 20px;
  border-bottom: 1px solid;
}

</style>
<body>
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Clients Page</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      
    </ul>
    <a class="nav-link" href="/home">Home<span class="sr-only">(current)</span></a>
    <a class="btn btn-danger" href="/login">Logout<span class="sr-only">(current)</span></a>
  </div>
</nav>



<br>
<br>
<table class="text-center">
  <h6 style="color:red;">Warning: You can't delete a client if it is currently in a room</h6>
  <thead>
    <tr>
      <th scope="col">Client ID</th>
      <th scope="col">Client Name</th>
      <th scope="col">Address</th>
      <th scope="col">Phone Number</th>
      <th scope="col"><a style="font-size: 30px; text-align:right;" class="btn btn-warning" href="/addclient">+<span class="sr-only">(current)</span></a></th>
    </tr>
  </thead>
  @foreach ($users as $user)
  <tbody>
    <tr>
    <td>{{ $user->client_id}}</td>
    <td>{{ $user->client_name }}</td>
    <td>{{ $user->client_address }}</td>
    <td>{{ $user->client_phone }}</td>
    <td colspan="2">
    <form action = "/clientedit/{{ $user->client_id }}" method = "get">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
    <input class="btn btn-primary" type = 'submit' value = "Edit" />
    </form>
    <br>
    <form action = "/clientdelete/{{ $user->client_id }}" method = "get">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
    <input class="btn btn-danger" type = 'submit' value = "Delete" />
    </form>
  </td>
               
    </tr>
  </tbody>

  @endforeach
</table>
</body>
</html>