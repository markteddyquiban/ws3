-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2022 at 10:29 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etr`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `client_name` varchar(50) DEFAULT NULL,
  `client_address` varchar(50) DEFAULT NULL,
  `client_phone` varchar(50) DEFAULT NULL,
  `rooms_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `client_name`, `client_address`, `client_phone`, `rooms_id`) VALUES
(1, 'Teddy', 'Quiban', '09267555123', 1),
(2, 'Melody', 'San Rio', '0584', 2),
(3, 'Lorrainne', 'San Rio', '09167071447', 3),
(4, 'Usagi', 'Moon', '3051', 4),
(5, 'Number5', NULL, NULL, 5),
(6, 'Number6', NULL, NULL, 6),
(7, 'Number7', NULL, NULL, 7),
(8, 'Number8', NULL, NULL, 10),
(9, 'Number9', NULL, NULL, 8),
(10, 'Number10', NULL, NULL, 9),
(11, 'Number11', NULL, NULL, 11),
(12, 'Number12', NULL, NULL, 12),
(15, 'Kyle', 'Plaridel', '09123124', 0),
(17, 'Kuromi', 'San Rio', '0311', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `computed`
--

CREATE TABLE `computed` (
  `id` int(11) NOT NULL,
  `totale` int(11) DEFAULT NULL,
  `totalr` int(11) DEFAULT NULL,
  `finaldate` date DEFAULT NULL,
  `water` int(11) DEFAULT NULL,
  `rooms_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `computed`
--

INSERT INTO `computed` (`id`, `totale`, `totalr`, `finaldate`, `water`, `rooms_id`, `total`) VALUES
(1, 10000, 12000, '2022-06-11', 1000, 1, 23000),
(2, 200, 10000, '2022-06-04', 1000, 2, 11200),
(3, 230, 10000, '2022-06-19', 500, 3, 10730),
(4, NULL, NULL, NULL, NULL, 4, NULL),
(5, NULL, NULL, NULL, NULL, 5, NULL),
(6, NULL, NULL, NULL, NULL, 6, NULL),
(7, 2, 3, NULL, 4, 7, NULL),
(8, NULL, NULL, NULL, NULL, 8, NULL),
(9, NULL, NULL, NULL, NULL, 9, NULL),
(10, NULL, NULL, NULL, NULL, 10, NULL),
(11, NULL, NULL, NULL, NULL, 11, NULL),
(12, NULL, NULL, NULL, NULL, 12, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE `fee` (
  `id` int(11) NOT NULL,
  `water` int(11) DEFAULT NULL,
  `rent` int(11) DEFAULT NULL,
  `electric` int(11) DEFAULT NULL,
  `rooms_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`id`, `water`, `rent`, `electric`, `rooms_id`) VALUES
(1, 500, 5000, 20, 1),
(2, 500, 5000, 20, 2),
(3, 500, 5000, 20, 3),
(4, 500, 5000, 20, 4),
(5, 500, 5000, 20, 5),
(6, 500, 5000, 20, 6),
(7, 500, 5000, 20, 7),
(8, 500, 5000, 20, 8),
(9, 500, 5000, 20, 9),
(10, 500, 5000, 20, 10),
(11, 500, 5000, 20, 11),
(12, 500, 5000, 20, 12);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `payment_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_date`) VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL),
(6, NULL),
(7, NULL),
(8, NULL),
(9, NULL),
(10, NULL),
(11, NULL),
(12, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pl_admin`
--

CREATE TABLE `pl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `userpass` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pl_admin`
--

INSERT INTO `pl_admin` (`id`, `username`, `userpass`) VALUES
(1, 'qwerty', 'qwerty');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `rooms_id` int(11) NOT NULL,
  `rooms_no` varchar(50) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`rooms_id`, `rooms_no`, `payment_id`, `client_id`) VALUES
(0, NULL, NULL, NULL),
(1, '1', 1, 1),
(2, '2', 2, 2),
(3, '3', 3, 3),
(4, '4', 4, 4),
(5, '5', 5, 5),
(6, '6', 6, 6),
(7, '7', 7, 7),
(8, '8', 8, 8),
(9, '9', 9, 9),
(10, '10', 10, 10),
(11, '11', 11, 11),
(12, '12', 12, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `computed`
--
ALTER TABLE `computed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_computed_rooms` (`rooms_id`);

--
-- Indexes for table `fee`
--
ALTER TABLE `fee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_fee_rooms` (`rooms_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `pl_admin`
--
ALTER TABLE `pl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`rooms_id`),
  ADD KEY `FK_rooms_clients` (`client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `computed`
--
ALTER TABLE `computed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fee`
--
ALTER TABLE `fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pl_admin`
--
ALTER TABLE `pl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `rooms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `computed`
--
ALTER TABLE `computed`
  ADD CONSTRAINT `FK_computed_rooms` FOREIGN KEY (`rooms_id`) REFERENCES `rooms` (`rooms_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fee`
--
ALTER TABLE `fee`
  ADD CONSTRAINT `FK_fee_rooms` FOREIGN KEY (`rooms_id`) REFERENCES `rooms` (`rooms_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `FK_rooms_clients` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
