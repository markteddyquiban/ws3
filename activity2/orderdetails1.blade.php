<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OrderDetails</title>
</head>
<style>
    ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover {
  background-color: #111;
}
h1 {
    font:3em sans-serif;
    color: green;
}
</style>

<body>
<ul>
  <li><a >Mark</a></li>
  <li><a >Teddy</a></li>
  <li><a >Quiban</a></li>
  <li><a >BSIT-3B</a></li>
</ul>
<h1>OrderDetails Page</h1>
</body>
</html>