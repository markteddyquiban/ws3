<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/021a3893c0.js" crossorigin="anonymous"></script>

</head>
<style>
body {
    text-align: center;
    
}
form {
    display: inline-block;
    border-radius: 10px;
    margin: 10px;
    padding: 10px;
    border: 1px solid;
}
</style>
<body>


<br><br><br><br>
  <h2>Register</h2>
  <br>
<div class="form-group">
  <form action="register" method="post">
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <label>Full Name</label>
        <input type="text" name="name" class="form-control" placeholder="Enter Name">
        <br>
    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Enter Email">
    </div>
    <br>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Enter Password">
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a class="nav-link active"  href="login">Login</a>

</div>
  </form>

</body>
</html>