<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Admin</title>
</head>
<style>
body {
    text-align: center;
  
}

.content-table {
    
  border-collapse: collapse;
  margin: 25px 0;
  font-size: 0.9em;
  min-width: 400px;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}

.content-table thead tr {
  background-color: #009879;
  color: #ffffff;
  text-align: left;
  font-weight: bold;
}

.content-table th,
.content-table td {
  padding: 12px 15px;
}

.content-table tbody tr {
  border-bottom: 1px solid #dddddd;
}

.content-table tbody tr:nth-of-type(even) {
  background-color: #f3f3f3;
}

.content-table tbody tr:last-of-type {
  border-bottom: 2px solid #009879;
}

.content-table tbody tr.active-row {
  font-weight: bold;
  color: #009879;
}

.btn{
    align-self: right;
}

</style>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand">Appointments</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
    </ul>
    <a class="form-inline my-2 my-lg-0" href="login">Logout</a>
  </div>
</nav>

<table class="content-table">
            <thead>
                <tr>
        <th class="header">ID</th>
        <th class="header">Name</th>
        <th class="header">Address</th>
        <th class="header">Date</th>
        <th class="header">Time</th>
        <th class="header">Verification</th>
        <th class="header">Cancel</td>
                </tr>
                @foreach ($appointment as $list)
                <tr>
                <td>{{ $list->id }}</td>
                <td>{{ $list->a_name }}</td>
                <td>{{ $list->a_address }}</td>
                <td>{{ $list->a_date }}</td>
                <td>{{ $list->a_time }}</td>
                <td>{{ $list->verification }}</td>
                <td><a class="btn btn-danger" href = 'admin/{{ $list->id }}'>Cancel</a></td>
                </tr>
@endforeach
            </thead>
            <tbody>
            </tbody>
        </table>
</body>
</html>
