<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\StudInsertController;
use App\Http\Controllers\StudUpdateController;
use App\Http\Controllers\StudDeleteController;
use App\Http\Controllers\CRUDController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/laravel', function () {
    return view('laravel');
});

Route::get('/activity1', function () {
    return view('activity1');
});

Route::get('/activity1/registration', function () {
    return view('/activity1/registration');
});

Route::get('/activity1/login', function () {
    return view('/activity1/login');
});

Route::get('/activity1/contactus', function () {
    return view('/activity1/contactus');
});
Route::get('/activity1/about', function () {
    return view('/activity1/about');
});
Route::get('/activity1/homepage', function () {
    return view('/activity1/homepage');
});

Route::get('/research/index', function () {
    return view('/research/index');
});

Route::get('/research/print', function () {
    return view('/research/print');
});

Route::view("/",'student');

Route::Get('/',function(){return view('student');});
Route::get('student/details',function(){
    $url=route('student.details');
    return $url;
})->name('student.details');
/*
Route::get('costumer/{id}/{name}/{address}', function () {  
    return view('/activity2/costumer1');
});
Route::get('item/{itemno}/{name}/{price}', function () {  
    return view('/activity2/item1');
});
Route::get('order/{customerid}/{name}/{orderno}/{date}', function () {  
    return view('/activity2/order1');
});
Route::get('orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}', function () {  
    return view('/activity2/orderdetails1');
});*/

Route::get('costumer/{id}/{name}/{address}/{age?}', function ($id=NULL,$name=NULL,$address=NULL,$age=NULL) {  
    return view('/midterm/costumer');
});
Route::get('item/{itemno}/{name}/{price}', function ($itemno=NULL,$name=NULL,$price=NULL) {  
    return view('/midterm/item');
});
Route::get('order/{customerid}/{name}/{orderno}/{date}', 
function ($costumerid=NULL,$name=NULL,$orderno=NULL,$date=NULL) {  
    return view('/midterm/order');
});
Route::get('orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}/{receipt?}', function 
($transno=NULL,$orderno=NULL,$itemid=NULL,$name=NULL,$price=NULL,$qty=NULL,$receipt=NULL) {  
    return 'Mark Teddy Quiban BSIT-3B Total Price:'.($price*$qty).'Receipt:'.$receipt.'';
});

Route::get('insert',[StudInsertController::class, 'insertform']);
Route::post('create',[StudInsertController::class, 'insert']);

Route::get('view-records',[StudViewController::class, 'index']);

Route::get('edit-records',[StudUpdateController::class, 'index']);
Route::get('edit/{id}',[StudUpdateController::class, 'show']);
Route::post('edit/{id}',[StudUpdateController::class, 'edit']);

Route::get('delete-records',[StudDeleteController::class, 'index']);
Route::get('delete/{id}',[StudDeleteController::class, 'destroy']);


Route::get('login',[CRUDController::class, 'index']);
Route::post('login',[CRUDController::class, 'login']);

Route::get('register',[CRUDController::class, 'view_register']);
Route::post('register',[CRUDController::class, 'register']);


Route::get('main',[CRUDController::class, 'view_main']);
Route::post('main',[CRUDController::class, 'main']);

Route::get('list',[CRUDController::class, 'view_list']);
Route::post('list',[CRUDController::class, 'list']);
Route::get('list/{id}',[CRUDController::class, 'list_delete']);

Route::get('admin',[CRUDController::class, 'view_admin']);
Route::get('admin/{id}',[CRUDController::class, 'list_delete_admin']);