<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CRUDController extends Controller
{
    //
    function index(){
        return view('crudactivity/login');
    }
    function login(Request $req){
        $email = $req->input('email');
        $password=$req->input('password');
        $results = DB::select('select * from user where email = :email', ['email' => $email]);
        $check = DB::select('select * from user where email = :email', ['email' => $email]);
        if($email=="admin" && $password=="admin"){
            return redirect('admin');
        }

        if($results==$check){
            return redirect('main');
        }
        

    }
    function view_register(){
        return view('crudactivity/register');
    }


    function register(Request $req){

        $name = $req->input('name');
        $email = $req->input('email');
        $password = $req->input('password');
        DB::insert('insert into user (name, email,password) values (?, ?, ?)', [$name,$email,$password]);
        echo "Registered successfully.<br/>";
        echo '<a href = "/login">Click Here</a> to go to login page.';

    }


    function view_main(){
        return view('crudactivity/main');
    }

    function view_admin(){
        $appointment = DB::select('select * from appointment');
        return view('crudactivity/admin',['appointment'=>$appointment]);
    }

    function list_delete_admin($id){

        DB::update('update appointment set verification=? where id = ?',["Cancelled",$id]);
        echo "Record cancelled successfully.<br/>";
        echo '<a href = "/admin">Click Here</a> to go back.';
    }
    

    function main(Request $req){
        $name = $req->input('name');
        $number = $req->input('number');
        $address= $req->input('address');
        $date= $req->input('date');
        $fdate = date("Y-m-d", strtotime($date));
        $time = $req->input('time');
        DB::insert('insert into appointment (a_name, a_number,a_address,a_date,a_time,verification) values (?, ?, ?, ?, ?, ?)', [$name,$number,$address,$fdate,$time,"Confirmed"]);
        echo "Appointment added successfully.<br/>";
        echo '<a href = "/main">Click Here</a> to go to back.';
    }

    function view_list(){
        $appointment = DB::select('select * from appointment');
        return view('crudactivity/list',['appointment'=>$appointment]);
    }

    public function list_delete($id) {
        DB::delete('delete from appointment where id = ?',[$id]);
        echo "Record deleted successfully.<br/>";
        echo '<a href = "/list">Click Here</a> to go back.';
     }
    
}
