<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class EndTermController extends Controller
{
    //index
    function index(){
        return view('etr/login');
    }

    //admin login
    function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        $users= DB::select('SELECT *
        FROM pl_admin
        WHERE username=? AND userpass=?;',[$username,$password]);
        
        if(!empty($users)){
            return redirect('/home');
        }
        else{
            return back()->with('error','Wrong Login Credentials');
        }
    }
    //view home
    function view_home(){
        $users= DB::select('SELECT *
        FROM clients
        INNER JOIN rooms
        ON rooms.client_id = clients.client_id
        INNER JOIN computed
        ON rooms.rooms_id = computed.rooms_id;');
        return view('etr/home',['users'=>$users]);
    }

    //view settings
    function view_settings(){
        return view('etr/settings');
    }
    //view fee settings
    function view_feesettings(){
        return view('etr/feesettings');
    }
    function setfee(Request $request) {
        $water = $request->input('water');
        $rent = $request->input('rent');
        $electric = $request->input('electric');
        DB::update('UPDATE fee
        SET water=?,rent=?,electric=?',[$water,$rent,$electric]);
        echo "Fee updated successfully.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.';
        
        }
    //view admin settings
    function view_adminsettings(){
        return view('etr/adminsettings');
    }
    function setadmin(Request $request) {
        $username = $request->input('username');
        $userpass = $request->input('userpass');
        DB::update('UPDATE pl_admin
        SET username = ?
        , userpass = ?
        WHERE id=1;',[$username,$userpass]);
        echo "Admin Settings updated successfully.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.';
        
        }
    //view clients
    function view_clientspage(){
        $users = DB::select('SELECT * FROM clients
        ;');
    return view('etr/clients',['users'=>$users]);
    }
    function setclientedit(Request $request,$id) {
        $name = $request->input('name');
        $address = $request->input('address');
        $phone = $request->input('phone');
        DB::update('UPDATE clients 
        SET client_name=?,client_address=?,client_phone=?
        WHERE client_id=?;',[$name,$address,$phone,$id]);
        
        echo "Client edited successfully.<br/>";
        echo '<a href = "/clients">Click Here</a> to go back.';
        
        }
 
    //view addclients
    function view_addclients(){
        return view('etr/addclients');
    }
    function setaddclients(Request $request) {
        $name = $request->input('name');
        $address = $request->input('address');
        $phone = $request->input('phone');
        DB::update('INSERT INTO clients (client_name, client_address, client_phone)
        VALUES (?,?,?);',[$name,$address,$phone]);
        echo "New Client successfully added.<br/>";
        echo '<a href = "/clients">Click Here</a> to go back.';
        
        }
    //view c

    //view billing
    function view_billing($id){
        $users = DB::select('SELECT *
        FROM rooms
        INNER JOIN clients
        ON clients.rooms_id = rooms.rooms_id
        INNER JOIN fee
        ON rooms.rooms_id = fee.rooms_id
        WHERE rooms.rooms_id=?
        ;',[$id]);
    return view('etr/billing',['users'=>$users]);
    }

    function compute(Request $request,$id){
        $date = $request->input('date');
        $kwh = (int)$request->input('kwh');
        $rent = (int)$request->input('rent');
        $water = (int)$request->input('water');
        $electric= (int)$request->input('electric');
        $months= (int)$request->input('months');
        $totale= (int)($kwh*$electric);
        $totalr=(int)($rent*$months);
        $totalw=(int)($water*$months);
        $finaldate=date("Y-m-d", strtotime($date));
        $finaltotal=$totale+$totalr+$totalw;
        DB::update('UPDATE computed
        SET totale = ?
        ,finaldate=?
        , totalr = ?
        ,total=?
		  ,water=?
        WHERE id=?;',[$totale,$finaldate,$totalr,$finaltotal,$totalw,$id]);

        $users=DB::select('SELECT * FROM computed WHERE id=?
        ;',[$id]);
    return view('etr/computed',['users'=>$users]);
    }



    //view room
    function view_room($id){
        $users = DB::select('SELECT *
        FROM rooms
        INNER JOIN clients
        ON clients.rooms_id = rooms.rooms_id
        INNER JOIN computed
        ON computed.rooms_id = rooms.rooms_id 
        WHERE rooms.rooms_id=?
        ;',[$id]);
    return view('etr/viewroom',['users'=>$users]);
    }
     //view clientedit
     function view_clientedit($id){ 
        $users = DB::select('SELECT *
        FROM clients
        WHERE client_id=?
        ;',[$id]);
    return view('etr/clientedit',['users'=>$users]);
    }
     //view editrooms
     function view_editroom($id){
        $users = DB::select('SELECT *
        FROM rooms
        WHERE rooms_id=?
        ;',[$id]);
    return view('etr/editroom',['users'=>$users]);
    }

     function editroom(Request $request,$id) {
        $client = $request->input('id');
         DB::update('UPDATE rooms
         SET client_id=?
         WHERE rooms_id=?;',[$client,$id]);
        
        echo "Record updated successfully.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.';
        
        }

        function clientdelete($id) {
           
            DB::delete('delete from clients where client_id = ?',[$id]);
            echo "Client deleted successfully.<br/>";
            echo '<a href = "/clients">Click Here</a> to go back.';
            
            }









}
