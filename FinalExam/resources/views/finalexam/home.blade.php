<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<style>
  *{
    font-family: sans-serif;
  }

body {
    text-align: center;
    
}
table {
    width:50%;
    table-layout: fixed;
    display: inline-block;
    border-radius: 10px;
    padding: 20px;
    border: 1px solid;
    margin-right: auto;
    margin-left: auto;
    font-size: 15px;
   
}
td {
  margin: 10px;
  padding: 20px;
  border-bottom: 1px solid;

}

th {
  margin: 10px;
  padding: 20px;
  border-bottom: 1px solid;
}


</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Home Page</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      
    </ul>

    <a class="btn btn-danger" href="login">Logout<span class="sr-only">(current)</span></a>
  </div>
</nav>
<body>
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

<br>
<br>
<br>
<br>
<br>
<table class="text-center">
  <thead>
    <tr>
      <th scope="col">name</th>
      <th scope="col">Price</th>
      <th scope="col">Stocks</th>
      <th scope="col">Available?</th>
      <th scope="col"></th>
    </tr>
  </thead>
  @foreach ($users as $user)
  <tbody>
    <tr>
    <td>{{ $user->name}}</td>
    <td>{{ $user->price}}</td>
    <td>{{ $user->stocks }}</td>
    <td>Available</td>
    <td colspan="2">
    <form action = "/request/{{ $user->id }}" method = "get">
    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
    <input class="btn btn-primary" type = 'submit' value = "Request" />
    </form>
    <br>
  </td>
               
    </tr>
  </tbody>

  @endforeach
</table>

</body>
</html>