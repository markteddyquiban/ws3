
<?php
if(isset($_GET["pindot"])){
    echo '<script>alert("Hello po")</script>';
  }
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>About</title>

        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        
        <style>
     


     *{
	padding: 0;
	margin: 0;
	font-family: 'Josefin Sans', sans-serif;
	box-sizing: border-box;
} 
.about{
	width: 100%;
	padding: 100px 0px;
	background-color: #191919;
}
.about img{
	height: auto;
	width: 430px;
}
.about-text{
  
	width: 550px;
}
.main{
	width: 1130px;
	max-width: 95%;
	margin: 0 auto;
	display: flex;
	align-items: center;
	justify-content: space-around;
  
}
.about-text h2{
  
	color: white;
	font-size: 75px;
	text-transform: capitalize;
	margin-bottom: 20px;
}
.about-text h5{
	color: white;
	letter-spacing: 2px;
	font-size: 22px;
	margin-bottom: 25px;
	text-transform: capitalize;
}
.about-text p{
	color: white;
  border-style: solid;
  border-color: red;
  padding:10px;
	letter-spacing: 1px;
	line-height: 28px;
	font-size: 18px;
	margin-bottom: 45px;
}
nav{
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding-top: 45px;
	padding-left: 8%;
	padding-right: 8%;
}
span{
	color:#fcfc ;
}
nav ul li{
	list-style-type: none;
	display: inline-block;
	padding: 10px  25px;
}
nav ul li a{
	color: red;
	text-decoration: none;
	font-weight: bold;
	
}
nav ul li a:hover{
	color: blue;
	transition: .4s;
}
.logo{
	color: red;
	font-size: 35px;
	letter-spacing: 1px;
	cursor: pointer;
}
        </style>
    </head>
    <body >
    <div class="top">
		<nav>
			<h2 class="logo">Portfo<span>lio</span></h2>
			<ul>
				<li><a href="homepage">Home</a></li>
				<li><a href="about">About Me</a></li>
				<li><a href="login">Login</a></li>
				<li><a href="registration">Register</a></li>
				<li><a href="contactus">Contact Me</a></li>
			</ul>
		</nav>
    </div>
    <section class="about">
		<div class="main">
			<img src="https://scontent.fbag1-2.fna.fbcdn.net/v/t1.6435-9/52635813_1970327429928891_5771473925905055744_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=PIyHb4VHFvgAX83gBCk&_nc_ht=scontent.fbag1-2.fna&oh=00_AT9wfwuaKIZiARzXSPi3bR4ZsVR-SKtM_T1ZuvWCyKT46g&oe=6259797F">
			<div class="about-text">
				<h2>About Me</h2>
				<h5> IT Student <span>& Gamer</span></h5>
				<p>I am an IT student and also a gamer. I play games such as League of Legends, Valorant, Call of Duty Mobile, LoL:WildRift, and Dota 2. My favourite part of being an IT student for now is working on databases.</p>
			</div>
		</div>
	</section>
  <section class="about">
		<div class="main">
			<img src="https://scontent.fbag1-2.fna.fbcdn.net/v/t39.30808-6/241560232_2601596080135353_731961006291317474_n.jpg?_nc_cat=104&ccb=1-5&_nc_sid=e3f864&_nc_ohc=hiQbrF453OsAX8nKrv3&_nc_ht=scontent.fbag1-2.fna&oh=00_AT_RMgC_ha4ODdh0E2u8v4Ob2fTq8H0wFKL7tipnK0bx5A&oe=6238FF05">
			<div class="about-text">
				<h5> Relationship Status? <span>TAKEN</span></h5>
				<p>This is my girlfriend with me in the picture, her full name is Alexandria Lorrainne Aquino</p>
			</div>
		</div>
	</section>
  <section class="about">
		<div class="main">
			<img src="https://scontent.fbag1-2.fna.fbcdn.net/v/t39.30808-6/244381405_2623293554632272_492917936171553502_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=174925&_nc_ohc=mo-hkD2qKMMAX_FEyFB&_nc_ht=scontent.fbag1-2.fna&oh=00_AT8y5HUPjjhKEsLrnEf--a-r2ROKxlJk-eRoOev8tvqEzw&oe=623A12D8">
			<div class="about-text">
				<h5> I love <span>dogs</span></h5>
				<p>Yes I really love dogs, this was one of them, his name is Putot but now he is in a better place.</p>
			</div>
		</div>
	</section>
    </body>
</html>
