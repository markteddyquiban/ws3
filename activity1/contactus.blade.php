

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Homepage</title>

        
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <style>
          *{
	padding: 0;
	margin: 0;
	font-family: 'Josefin Sans', sans-serif;
	box-sizing: border-box;
} 
nav{
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding-top: 45px;
	padding-left: 8%;
	padding-right: 8%;
  background-color: grey;
}
span{
	color:#fcfc ;
}
nav ul li{
	list-style-type: none;
	display: inline-block;
	padding: 10px  25px;
}
nav ul li a{
	color: pink;
	text-decoration: none;
	font-weight: bold;
	
}
nav ul li a:hover{
	color: pink;
	transition: .4s;
}
.top{
	height: 100vh;
	width: 100%;
	background-image: url(https://scontent.fbag1-2.fna.fbcdn.net/v/t1.6435-9/51956014_1970327849928849_1571654079795429376_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=19026a&_nc_ohc=vsgydtDyQXYAX9wBRFg&tn=0F7gi8qesWjPeIsY&_nc_ht=scontent.fbag1-2.fna&oh=00_AT8lyNo07QU58N4WQXr0Z31GDQcJyVwixqBX_Ohy_Eg9ZA&oe=625AE265);
	background-size: cover;
	background-position: center;
}
.content{
	position: absolute;

	left:3%;
  bottom:5px;
	transform: translateY(-50%);
}
h1{
	color: white;
	margin: 20px 0px 20px;
	font-size: 75px;
}
h3{
	color: white;
	font-size: 25px;
	margin-bottom: 50px;
}
h4{
	color:#fcfc ;
	letter-spacing: 2px;
	font-size: 20px;
}
.logo{
	color: white;
	font-size: 35px;
	letter-spacing: 1px;
	cursor: pointer;
}
.contact-me{
	width: 100%;
	height: 290px;
	background: #191919;
	display: flex;
	align-items: center;
	flex-direction: column;
	justify-content: center;
}
.contact-me p{
	color: white;
	font-size: 30px;
	font-weight: bold;
	margin-bottom: 25px;
}
.contact-me .button-two{
	background-color:#f9004d;
	color: white;
	text-decoration: none;
	border: 2px solid transparent;
	font-weight: bold;
	padding: 13px 30px;
	border-radius: 30px;
	transition: .4s; 
}
.contact-me .button-two:hover{
	background-color: transparent;
	border: 2px solid #f9004d;
	cursor: pointer;
} 
        </style>
    </head>
    <body>
    <div class="top">
		<nav>
			<h2 class="logo">Portfo<span>lio</span></h2>
			<ul>
				<li><a href="homepage">Home</a></li>
				<li><a href="about">About Me</a></li>
				<li><a href="login">Login</a></li>
				<li><a href="registration">Register</a></li>
				<li><a href="contactus">Contact Me</a></li>
			</ul>
		</nav>

    <div class="contact-me">
		<p>Email:  shikei031@gmail.com</p><br>
    <p>Contact No.:  09267555123</p>
		<a class="button-two" href="#">Send a message</a>
	</div>
	</div>
  
    </body>
</html>
