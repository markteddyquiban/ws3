

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Homepage</title>

        
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <style>
          *{
	padding: 0;
	margin: 0;
	font-family: 'Josefin Sans', sans-serif;
	box-sizing: border-box;
} 
nav{
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding-top: 45px;
	padding-left: 8%;
	padding-right: 8%;
}
span{
	color:#fcfc ;
}
nav ul li{
	list-style-type: none;
	display: inline-block;
	padding: 10px  25px;
}
nav ul li a{
	color: white;
	text-decoration: none;
	font-weight: bold;
	
}
nav ul li a:hover{
	color: #f9004d;
	transition: .4s;
}
.top{
	height: 100vh;
	width: 100%;
	background-image: url(https://scontent.fbag1-2.fna.fbcdn.net/v/t1.6435-9/51956014_1970327849928849_1571654079795429376_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=19026a&_nc_ohc=vsgydtDyQXYAX9wBRFg&tn=0F7gi8qesWjPeIsY&_nc_ht=scontent.fbag1-2.fna&oh=00_AT8lyNo07QU58N4WQXr0Z31GDQcJyVwixqBX_Ohy_Eg9ZA&oe=625AE265);
	background-size: cover;
	background-position: center;
}
.content{
	position: absolute;

	left:3%;
  bottom:5px;
	transform: translateY(-50%);
}
h1{
	color: white;
	margin: 20px 0px 20px;
	font-size: 75px;
}
h3{
	color: white;
	font-size: 25px;
	margin-bottom: 50px;
}
h4{
	color:#fcfc ;
	letter-spacing: 2px;
	font-size: 20px;
}
.logo{
	color: white;
	font-size: 35px;
	letter-spacing: 1px;
	cursor: pointer;
}
        </style>
    </head>
    <body>
    <div class="top">
		<nav>
			<h2 class="logo">Portfo<span>lio</span></h2>
			<ul>
				<li><a href="homepage">Home</a></li>
				<li><a href="about">About Me</a></li>
				<li><a href="login">Login</a></li>
				<li><a href="registration">Register</a></li>
				<li><a href="contactus">Contact Me</a></li>
			</ul>
		</nav>

		<div class="content">
			<h4>Hi, my name is</h4>
			<h1>Mark Teddy <span>Quiban</span></h1>
			<h3>BSIT 3B student</h3>
			
		</div>
	</div>
    </body>
</html>
