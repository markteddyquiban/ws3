<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class finalexamcontroller extends Controller
{
    function index(){
        return view('finalexam/login');
    }
    
    function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');

        if($username=="admin"&&$password=="admin"){
            return redirect('/admin');
        }
        else{
        $username = $request->input('username');
        $password = $request->input('password');
        $users= DB::select('SELECT *
        FROM users
        WHERE username=? AND userpass=?;',[$username,$password]);
        
        if(!empty($users)){
            return redirect('/home');
        }
        else{
            return back()->with('error','Wrong Login Credentials');
        }
    }
    }

    function view_register(){
        return view('finalexam/register');
    }
    function register(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        $email = $request->input('email');
        $confirm = $request->input('confirm');
        $users= DB::select('INSERT INTO `finalexam`.`users` (`username`, `email`, `userpass`) 
        VALUES (?, ?, ?);',[$username,$email,$password]);    
        return redirect('/login');
    }

    function view_home(){
        $users= DB::select('SELECT *
        FROM items;');
        return view('finalexam/home',['users'=>$users]);
    }

    function view_admin(){
        $users= DB::select('SELECT *
        FROM users;');
        $trans= DB::select('SELECT *
        FROM transactions;');
        $items= DB::select('SELECT *
        FROM items;');
        return view('finalexam/admin',['users'=>$users,'items'=>$items,'trans'=>$trans]);
    }

    function view_request($id){
        $users= DB::select('SELECT *
        FROM items WHERE id=?;',[$id]);
        return view('finalexam/request',['users'=>$users]);
    }

    function request(Request $request,$id){
      
    }



}
