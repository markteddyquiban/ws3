<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Settings</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<style>
    body {
    text-align: center;
    
}
table {
    display: inline-block;
    border-radius: 10px;
    margin: 10px;
    padding: 50px;
    border: 1px solid;
}
table button{
  border-radius:1px solid;
}
td{
  margin: 10px;
  padding: 20px;
}
th {
  margin: 10px;
  padding: 20px;
  border-bottom: 1px solid;
}
</style>
<body>
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Settings Page</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
       
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      
    </ul>
    <a class="nav-link" href="/clients">Clients<span class="sr-only">(current)</span></a>
    <a class="nav-link" href="/home">Home<span class="sr-only">(current)</span></a>
    <a class="btn btn-danger" href="/login">Logout<span class="sr-only">(current)</span></a>
  </div>
</nav>
<br>
<br>

    <table class="text-center">
  <thead>
    <tr>
      <th><a href="feesettings" class="btn btn-secondary">Fee Settings</a></th>
    </tr>
  </thead>
  <tbody>
    <br>
    <tr>
    <td><a href="adminsettings" class="btn btn-danger">Admin Settings</a></td>          
    </tr>
  </tbody>
</table>
</body>
</html>